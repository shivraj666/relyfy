﻿using System;

using Xamarin.Forms;

namespace RelyFyCare
{
    public class OpenShiftResponse
    {

        public string address { get; set; }
        public string allDay { get; set; }
        public string careClientName { get; set; }
        public string endTime { get; set; }
        public string fromDate { get; set; }
        public string startTime { get; set; }
        public string toDate { get; set; }
       
    }
}

