﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelyFyCare.Models
{
   public class LoginResponseModel
    {

        public string name { get; set; }
        public string clientId { get; set; }
        public string clientIdentifier { get; set; }
        public string lastVisit { get; set; }
        public string employeeId { get; set; }
        public string employeeIdentifier { get; set; }
        public string imageUrl { get; set; }
        public string emailId { get; set; }
        public string contactNo { get; set; }
        public string address { get; set; }
        public string clientLocationId { get; set; }
        public string city { get; set; }
        public string companyName { get; set; }
    }
}
