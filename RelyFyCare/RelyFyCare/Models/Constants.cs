﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelyFyCare.Models
{
  public class Constants
    {
        public static bool isDev = true;
       // public static string baseUrl = "http://192.168.1.106:8092/WebServicesRelyfyCareUUID/api";
        public static string baseUrl = "http://demowsrelyfy.azurewebsites.net/WebServicesRelyfyCareUUID/api";


       public static string sContentType = "application/json"; // or application/xml




        //METODS//
        public static string getProfileData = "/CaregiverContactInformation/getCargiverContactInfoByUUID";
        public static string sendUnAvailabilityData = "/Unavailability/addUnavailability";
        public static string getUnAvailabilityData = "/Unavailability/getUnavailability";
        public static string sendOpenShiftMSG = "/messages/sendMessage";
        public static string shiftCareGiver = "/Shift/getPreviousShiftByCaregiver";

    }
}
