﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelyFyCare.Models
{
   public class ChangePasswordValidationModel
    {
        public int Id { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

        public ChangePasswordValidationModel() { }

        public ChangePasswordValidationModel(String OldPassword, String NewPassword, String ConfirmPassword)
        {
            this.OldPassword = OldPassword;
            this.NewPassword = NewPassword;
            this.ConfirmPassword = ConfirmPassword;
        }
        public bool CheckInformation()
        {
            if (!this.OldPassword.Equals("") && !this.NewPassword.Equals("") && !this.ConfirmPassword.Equals(""))
                return true;
            else
                return false;
        }
    }
}
