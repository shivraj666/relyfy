﻿using System;

using Xamarin.Forms;

namespace RelyFyCare
{
    public class UnavailabilityResponseModel
    {


        public string clientLocationId { get; set; }
        public string employeeIdentifier { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string endDate { get; set; }
        public string startDate { get; set; }
       


    }
}

