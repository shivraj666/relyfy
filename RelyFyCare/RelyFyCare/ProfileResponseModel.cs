﻿using System;

using Xamarin.Forms;

namespace RelyFyCare
{
    public class ProfileResponseModel 
    {

        public string firstName { get; set; }
        public string ssn { get; set; }
        public string caregiverId { get; set; }
        public string caregiverStatus { get; set; }
        public string imageUrl { get; set; }
        public string lastName { get; set; }
        public string office { get; set; }
        public string language { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string maritalStatus { get; set; }
        public string startOfCase { get; set; }
        public string isVeteran { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string homePhone { get; set; }
        public string officePhone { get; set; }
        public string email { get; set; }
        public string mobilePhone { get; set; }
        public string fax { get; set; }
        public string status { get; set; }
        public string officeNotes { get; set; }
        public string clientIdentifier { get; set; }
        public string uuid { get; set; }


    }
}

