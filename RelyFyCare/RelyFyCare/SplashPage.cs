﻿using RelyFyCare.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RelyFyCare
{
    class SplashPage : ContentPage
    {

        Image splashImage;

        public SplashPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            var sub = new AbsoluteLayout();
            splashImage = new Image()
            {
                Source = "splash.png",
                WidthRequest = 600,
                HeightRequest = 700
            };
            AbsoluteLayout.SetLayoutFlags(splashImage, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(splashImage, new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            sub.Children.Add(splashImage);

            this.BackgroundColor = Color.FromHex("#ffffff");
            this.Content = sub;

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await Task.Delay(3000);
            //await splashImage.ScaleTo(100, 2000);
            //await splashImage.ScaleTo(2.5, 1500, Easing.Linear);
            //await splashImage.ScaleTo(150, 1200, Easing.Linear);


            MoveNext();
        }

        private void MoveNext()
        {
            Application.Current.MainPage = new NavigationPage(new UserType());
                 NavigationPage.SetHasNavigationBar(this, false);

            //{
            //if (Application.Current.Properties.ContainsKey("sessionId") || !Application.Current.Properties.ContainsKey("sessionId").Equals(""))
            //{
            //    // Application.Current.MainPage = new NavigationPage(new MainPage());
            //    Navigation.PushModalAsync(new MainPage());
            //    NavigationPage.SetHasNavigationBar(this, false);
            //}
            //else
            //{
            //    Application.Current.MainPage = new NavigationPage(new UserType());
            //    NavigationPage.SetHasNavigationBar(this, false);
            //}
        }

    }
}