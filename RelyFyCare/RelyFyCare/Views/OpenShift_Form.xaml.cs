﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RelyFyCare.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenShift_Form : ContentPage
    {
       

        string strEmployeeUUID = "";
        string strSessionId = "";
        string strEmployeeIdentifier = "";
        string strClientLocationId = "";
        string strClientUUID = "";
        string strClientIdentifier = "";
        string strName = "";
      
        String str = "";
      

        public OpenShift_Form()
        {
            InitializeComponent();
           
            // myList.ItemsSource = res.DataList;
            str = Application.Current.Properties.ContainsKey("sessionId").ToString();

            LoginData();
            //CallServiceAsync();
        }



       
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            CallSMSAsync();

        }

        private void LoginData()
        {

            strEmployeeUUID = LoginPage.strEmployeeUUID;
            strSessionId = LoginPage.strSessionId;
            strEmployeeIdentifier = LoginPage.strEmployeeIdentifier;
            strClientLocationId = LoginPage.strClientLocationId;
            strClientUUID = LoginPage.strClientId;
            strClientIdentifier = LoginPage.strClientIdentifier;
            strName = LoginPage.strCaregiverName;

            CallServiceAsync();
        }

        public async Task CallServiceAsync()
        {


            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("uuid",LoginPage.strEmployeeUUID)

        });


                var request = await cl.PostAsync(Constants.baseUrl + "/Shift/getOpenShift", formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseShift res = JsonConvert.DeserializeObject<JsonResponseShift>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    //await DisplayAlert("Open Shift", res.ResponseMessage, "Ok");

                    myList.ItemsSource = res.list;


                }
                else
                {
                    await DisplayAlert("Open Shift", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


        }


        public async Task CallSMSAsync()
        {


            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("sessionId",strSessionId),
                    new KeyValuePair<string,string>("patientId",LoginPage.strClientId),
                    new KeyValuePair<string,string>("fromIdentifier",strEmployeeIdentifier),
                    new KeyValuePair<string,string>("toIdentifier",strClientIdentifier),
                    new KeyValuePair<string,string>("clientLocationId",strEmployeeUUID),

                    new KeyValuePair<string,string>("message",""),
                    new KeyValuePair<string,string>("messageTitle",""),
                    new KeyValuePair<string,string>("photoUrl",""),


        });


                var request = await cl.PostAsync(Constants.baseUrl + Constants.sendOpenShiftMSG, formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseShift res = JsonConvert.DeserializeObject<JsonResponseShift>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    await DisplayAlert("Open Shift", res.ResponseMessage, "Ok");

                }
                else
                {
                    await DisplayAlert("Open Shift", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


        }


        private class JsonResponseShift
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }



            public OpenShiftResponse[] list { get; set; }


        }


        protected override bool OnBackButtonPressed()
        {
            // If you want to continue going back
            base.OnBackButtonPressed();
            return false;

            // If you want to stop the back button
            // return true;

        }
    }
}