﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using RelyFyCare.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profile_Form : ContentPage
    {


      
        string strEmployeeUUID = "";
        string strSessionId = "";
        string strEmployeeIdentifier = "";
        string strClientLocationId = "";
        string strClientUUID = "";
        string strClientIdentifier = "";
        string strName = "";
      

        string strFName = "";
        string strlastName = "";
        string strGender = "";
        string strLanguage = "";
        string strDob = "";
        string strAddress1 = "";
        string strAddress2 = "";
        string strCity = "";
        string strState = "";
        string strZip = "";
        string strHomePhone = "";
        string strOfficePhone = "";
        string strFax = "";
        string strEmailId = "";
        string strOfficeNotes = "";
        string strOccupation = "";
        string strSSN = "";
        string strStatus = "";

       // String sessionId = "";
       // String employeeUniqueId = "";

        List<KeyValuePair<string, string>> requestKeyValuePair;

        public Profile_Form()
        {
            InitializeComponent();

            LoginData();

           // CallServiceAsync();

            //takePhoto.Clicked += async (sender, args) =>
            //{

            //    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            //    {
            //        await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
            //        return;
            //    }

            //    var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            //    {
            //        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
            //        Directory = "Sample",
            //        Name = "test.jpg"
            //    });

            //    if (file == null)
            //        return;

            //    await DisplayAlert("File Location", file.Path, "OK");

            //    image.Source = ImageSource.FromStream(() =>
            //    {
            //        var stream = file.GetStream();
            //        file.Dispose();
            //        return stream;
            //    });
            //};

            CameraButton.Clicked += CameraButton_Clicked;
        }


        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });

            if (photo != null)
                PhotoImage.Source = ImageSource.FromStream(() => { return photo.GetStream(); });
        }


        private void LoginData()
        {
           
            strEmployeeUUID = LoginPage.strEmployeeUUID;
            strSessionId = LoginPage.strSessionId;
            strEmployeeIdentifier = LoginPage.strEmployeeIdentifier;
            strClientLocationId = LoginPage.strClientLocationId;
            strClientUUID = LoginPage.strClientId;
            strClientIdentifier = LoginPage.strClientIdentifier;
            strName = LoginPage.strCaregiverName;
           
            CallServiceAsync();
        }

        public async Task CallServiceAsync()
        {


           



           // requestKeyValuePair = new List<KeyValuePair<string, string>>();
           // var jsonData = "{\"jwtToken\":\"" + strSessionId + "\",\"uuid\":\"" + strEmployeeUUID + "\"}";

            using (var cl = new HttpClient())
            {
                
                string sContentType = "application/json"; // or application/xml

                JObject oJsonObject = new JObject();
                oJsonObject.Add("jwtToken", strSessionId);
                oJsonObject.Add("uuid", strEmployeeUUID);
              
              
                var request = await cl.PostAsync(Constants.baseUrl + Constants.getProfileData,
                                                 new StringContent(oJsonObject.ToString(), Encoding.UTF8, sContentType));

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseGetProfileData res = JsonConvert.DeserializeObject<JsonResponseGetProfileData>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;
                if (dataresponse.Equals("102"))
                {
                    //await DisplayAlert("Profile", res.ResponseMessage, "Ok");
                    foreach (var str in res.list)
                    {
                        strFName = str.firstName;
                        strlastName = str.lastName;
                        strGender = str.gender;
                        strLanguage = str.lastName;
                        strDob = str.dob;
                        strAddress1 = str.address;
                        strCity = str.city;
                        strState = str.state;
                        strZip = str.postalCode;
                        strHomePhone = str.homePhone;
                        strOfficePhone = str.officePhone;
                        strFax = str.fax;
                        strEmailId = str.email;
                        strOfficeNotes = str.officeNotes;
                        strStatus = str.status;

                        //if (strStartDate.Equals()) { }

                        Entry_FName.Text = strFName;
                        Entry_LastName.Text = strlastName;
                        Gender.Title = strGender;
                        Language.Title = strLanguage;
                        Entry_AddStreet.Text = strAddress1;
                        Entry_Address.Text = strAddress2;
                        Entry_City.Text = strCity;
                        Entry_State.Text = strState;
                        Entry_Zip.Text = strZip;
                        Entry_Phone.Text = strHomePhone;
                        Entry_PhOffice.Text = strOfficePhone;
                        Entry_Fax.Text = strFax;
                        Entry_Email.Text = strEmailId;
                        Entry_Notes.Text = strOfficeNotes;
                        Status.Title = strStatus;
                        Entry_SSN.Text = str.ssn;
                        //Occupation.Title = str.


                    }

                }
                else
                {
                    await DisplayAlert("Profile", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


        }



        private class JsonResponseGetProfileData
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }

        

            public ProfileResponseModel[] list { get; set; }


        }

    }
}