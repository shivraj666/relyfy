﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabPage1 : ContentPage
    {
        bool isTogged1;
        bool isTogged2;
        bool isTogged3;
        bool isTogged4;

        public TabPage1()
        {
            InitializeComponent();
        }

        private void OnTogged(Object sender, ToggledEventArgs e )
        {
            isTogged1 = e.Value;

            if (isTogged1.ToString().Equals("True"))
            {
                txtResult.Text = "Available";

            }
            else
            {
                txtResult.Text = "Unavailable";

            }
            //txtResult.Text = isTogged.ToString();

        }


        private void OnTogged1(Object sender, ToggledEventArgs e)
        {
            isTogged2 = e.Value;

            if (isTogged2.ToString().Equals("True"))
            {
                txtResult1.Text = "Available";

            }
            else
            {
                txtResult1.Text = "Unavailable";

            }
            //txtResult.Text = isTogged.ToString();

        }

        private void OnTogged2(Object sender, ToggledEventArgs e)
        {
            isTogged3 = e.Value;

            if (isTogged3.ToString().Equals("True"))
            {
                txtResult2.Text = "Available";

            }
            else
            {
                txtResult2.Text = "Unavailable";

            }
            //txtResult.Text = isTogged.ToString();

        }


        private void OnTogged3(Object sender, ToggledEventArgs e)
        {
            isTogged4 = e.Value;

            if (isTogged4.ToString().Equals("True"))
            {
                txtResult3.Text = "Available";

            }
            else
            {
                txtResult3.Text = "Unavailable";

            }
            //txtResult.Text = isTogged.ToString();

        }


        private void OnToggedAll(Object sender, ToggledEventArgs e)
        {
            bool isTogged = e.Value;

            if (isTogged.ToString().Equals("True"))
            {
                // txtResultAll.Text = "Available";
                isTogged1 = false;
                isTogged2 = false;
                isTogged3 = false;
                isTogged = false;
                txtResult.Text = "Unavailable";
                txtResult1.Text = "Unavailable";
                txtResult2.Text = "Unavailable";
                txtResult3.Text = "Unavailable";
                //IsToggled = "false";



            }
            else
            {
                // txtResultAll.Text = "Unavailable";

                isTogged1 = true;
                isTogged2 = true;
                isTogged3 = true;
                isTogged = true;
                txtResult.Text = "Available";
                txtResult1.Text = "Available";
                txtResult2.Text = "Available";
                txtResult3.Text = "Available";

            }
            //txtResult.Text = isTogged.ToString();

        }
    }


}