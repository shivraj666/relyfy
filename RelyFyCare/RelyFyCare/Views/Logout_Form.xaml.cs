﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Logout_Form : ContentPage
    {


        static readonly string dateFormat = "D";
        DatePicker fromDatePicker, toDatePicker;
        Label resultLabel;

        public Logout_Form()
        {
            InitializeComponent();

            fromDatePicker = new DatePicker
            {
                Format = dateFormat,
                HorizontalOptions = LayoutOptions.Center
            };

            fromDatePicker.DateSelected += OnDateSelected;

            toDatePicker = new DatePicker
            {
                Format = dateFormat,
                HorizontalOptions = LayoutOptions.Center
            };
            toDatePicker.DateSelected += OnDateSelected;

            resultLabel = new Label
            {
                Font = Font.SystemFontOfSize(NamedSize.Large),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };


            this.Content = new StackLayout
            {
                Children =
                {
                    new StackLayout
                    {
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalOptions = LayoutOptions.Center,
                        Children =
                        {
                            new Label
                            { Text = "Days Between Dates",
                                Font = Font.SystemFontOfSize(NamedSize.Large, FontAttributes.Bold),
                                TextColor = Color.Accent
                            },
                            new BoxView
                            {
                                HeightRequest = 3,
                                Color = Color.Accent
                            }
                        }
                    },

                    new StackLayout
                    {
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        Children =
                        {
                            new Label
                            {
                                Text = "From:",
                                HorizontalOptions = LayoutOptions.Center
                            },
                            fromDatePicker
                        }
                    }, // "To" DatePicker view.
                    new StackLayout {

                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        Children =
                        { new Label
                            {
                                Text = "To:",
                                HorizontalOptions = LayoutOptions.Center
                            },
                            toDatePicker
                        }
                    },
                    resultLabel
                }
            };

            OnDateSelected(null, null);

        }

        void OnDateSelected(object sender, DateChangedEventArgs args)
        {
            int days = (int)Math.Round((toDatePicker.Date - fromDatePicker.Date).TotalDays);
            resultLabel.Text = String.Format("{0:F0} day{1} between dates", days, days == 1 ? "" : "s");
        }
        }

}