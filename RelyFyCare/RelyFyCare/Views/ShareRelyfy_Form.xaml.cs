﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShareRelyfy_Form : ContentPage
    {
        public ShareRelyfy_Form()
        {
            InitializeComponent();
            //ShareButton.Clicked += ShareButton_Clicked;

            var filePath = DependencyService.Get<IFileStore>().GetFilePath();

            var share = DependencyService.Get<IShare>();

            share.Show("Title", "Message", filePath);

        }

        //private void ShareButton_Clicked(object sender, EventArgs e)
        //{
        //    var filePath = DependencyService.Get<IFileStore>().GetFilePath();

        //    var share = DependencyService.Get<IShare>();

        //    share.Show("Title", "Message", filePath);
        //}
}
}