﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RelyFyCare.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LastVisit_Form : ContentPage
    {
        string newFromDate = "";
        string newToDate = "";


        public LastVisit_Form()
        {
            InitializeComponent();

        }


        void OnbtnSubmit(object sender, EventArgs args) {  
           // lbldisp.Text = string.Format("Thank you. Your appointment is set for {0}.", fromDate.Date.ToString());
           
           

}  



        void Handle_Clicked(object sender, System.EventArgs e)
        {


            var fromdate = fromDate.Date.ToString();
            var todate = toDate.Date.ToString();


            DateTime dt=System.DateTime.Parse(fromdate);
             newFromDate =  dt.ToString("MM-dd-yyyy");
           

            DateTime dt2=System.DateTime.Parse(todate);
             newToDate =  dt2.ToString("MM-dd-yyyy");
           
            CallPastVisitAsync();
          


        }



        public async Task CallPastVisitAsync()
        {


            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("uuid",LoginPage.strEmployeeUUID),
                    new KeyValuePair<string,string>("fromDate", newFromDate),
                    new KeyValuePair<string,string>("toDate", newToDate),
                 

        });


                var request = await cl.PostAsync(Constants.baseUrl + Constants.shiftCareGiver, formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseShift res = JsonConvert.DeserializeObject<JsonResponseShift>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    await DisplayAlert("Past Visit", res.ResponseMessage, "Ok");

                }
                else
                {
                    await DisplayAlert("Past Visit", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


        }


        private class JsonResponseShift
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }



            public OpenShiftResponse[] list { get; set; }


        }
    
    }
}