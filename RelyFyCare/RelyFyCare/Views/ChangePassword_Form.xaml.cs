﻿using Newtonsoft.Json;
using RelyFyCare.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePassword_Form : ContentPage
    {

        String sessionId = "";
        String employeeUniqueId = "";


        public ChangePassword_Form()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, true);


            LoginData();
        }
        private void LoginData()
        {
            if (Application.Current.Properties.ContainsKey("sessionId"))
            {
                sessionId = Convert.ToString(Application.Current.Properties["sessionId"]);
                employeeUniqueId = Convert.ToString(Application.Current.Properties["employeeUniqueId"]);
            }

        }
        void CancelProcedureAsync(Object sender, EventArgs e)
        {


        }
        async Task ConfirmProcedureAsync(Object sender, EventArgs e)
        {

            ChangePasswordValidationModel user = new ChangePasswordValidationModel(Entry_OldPassword.Text, Entry_NewPassword.Text, Entry_RePassword.Text);

            if (user.CheckInformation())
            {

                //DisplayAlert("Login", "Login Success", "Okey!");

                await CallServiceAsync();

            }
            else
            {
                await DisplayAlert("Change Password", "Can't be null", "Okey!");

            }
        }

        public async Task CallServiceAsync()
        {


            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
            new KeyValuePair<string,string>("oldPassword",Entry_OldPassword.Text.ToString()),
            new KeyValuePair<string, string>("newPassword",Entry_NewPassword.Text.ToString()),
            new KeyValuePair<string, string>("sessionId",sessionId),
            new KeyValuePair<string, string>("id",employeeUniqueId)
        });


                var request = await cl.PostAsync(Constants.baseUrl + "/VeraPalRegistration/changeVeraPalPassword?", formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseChangePassword res = JsonConvert.DeserializeObject<JsonResponseChangePassword>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;
                if (dataresponse.Equals("102"))
                {
                    await DisplayAlert("Login", res.ResponseMessage, "Ok");

                }
                else
                {
                    await DisplayAlert("Change Password", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


        }


        private class JsonResponseChangePassword
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }

            public string EmployeeUniqueId { get; set; }

            public LoginResponseModel[] DataList { get; set; }


        }
}
}