﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RelyFyCare.Models;
using RelyFyCare.Views;
using Xamarin.Forms;

namespace RelyFyCare
{

    public partial class UnAvailable_Status_Form : ContentPage
    {
        string strStartTimeDate = "";
        string strEntTimeDate = "";
        string strDate = "";

        public UnAvailable_Status_Form(object sr)
        {

          

            NavigationPage.SetHasNavigationBar(this, true);

            InitializeComponent();

            strDate=  sr.ToString();

           


         


            //NavigationPage.SetBackButtonTitle(this, "go back");

            // NavigationPage.SetHasNavigationBar(this, false);
            // if (Device.OS == TargetPlatform.iOS || Device.OS == TargetPlatform.Android)
            //   NavigationPage.SetTitleIcon(this, "ic_keyboard_backspace_white_24dp.png");



            // lbldisp.Text = string.Format("Thank you. Your appointment is set for {0}.", arrivalTimePicker.Time.ToString());  
            // arrivalTimePicker.Time += OnTimeSelected;



        }


        void  OnbtnSubmitAsync(object sender, EventArgs args)
        {

            //lbldisp.Text = string.Format("Thank you. Your appointment is set for {0}.", arrivalTimePicker.Time.ToString());

            strStartTimeDate = strDate +" "+ arrivalTimePicker.Time.ToString();
            strEntTimeDate = strDate +" " + endTimePicker.Time.ToString();


            if (arrivalTimePicker.Time.ToString().Equals("12:00"))
            {
                DisplayAlert("Please select valid time", "Alert");
            }
            else if (endTimePicker.Time.Equals("12:00"))
            {

                DisplayAlert("Please select valid time", "Alert");
            }
            else
            {
                
                CallServiceAsync();

            }





        }



        private void DisplayAlert(string v1, string v2)
        {
            throw new NotImplementedException();
        }

        public async Task CallServiceAsync()
        {

            string strEmployeeUUID = LoginPage.strEmployeeUUID;
            string strSessionId = LoginPage.strSessionId; 
            string strEmployeeIdentifier = LoginPage.strEmployeeIdentifier;
            string strClientLocationId = LoginPage.strClientLocationId;
            string strClientUUID = LoginPage.strClientId;
            string strClientIdentifier = LoginPage.strClientIdentifier;
            string strName = LoginPage.strCaregiverName;



         
            
            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("EmployeeUUID",strEmployeeUUID),
                    new KeyValuePair<string, string>("sessionId",strSessionId),
                    new KeyValuePair<string, string>("employeeIdentifier", strEmployeeIdentifier),
                    new KeyValuePair<string, string>("clientLocationId", strClientLocationId),
                    new KeyValuePair<string, string>("TimeZone",""),
                    new KeyValuePair<string, string>("AllDay",""),
                    new KeyValuePair<string, string>("StartDate",strStartTimeDate),
                    new KeyValuePair<string, string>("EndDate",strEntTimeDate),
                    new KeyValuePair<string, string>("Recurrence",""),
                    new KeyValuePair<string, string>("Note",""),
                    new KeyValuePair<string, string>("clientUUID", strClientUUID),
                    new KeyValuePair<string, string>("clientIdentifier", strClientIdentifier),
                    new KeyValuePair<string, string>("caregiverName", strName)

                   

        });


                var request = await cl.PostAsync(Constants.baseUrl + Constants.sendUnAvailabilityData, formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseShift res = JsonConvert.DeserializeObject<JsonResponseShift>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    await DisplayAlert("UnAvailability", res.ResponseMessage, "Ok");

                    //Application.Current.MainPage = new NavigationPage(new HomePage());
                    //NavigationPage.SetHasNavigationBar(this, false);


                }
                else
                {
                    await DisplayAlert("Open Shift", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


  




        }


        private class JsonResponseShift
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }



            public LoginResponseModel[] list { get; set; }


        }


    }
    }

