﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserType : ContentPage
    {
        public UserType()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        void UserSignInProcedure(Object sender, EventArgs e)
        {

        }

        void UserLogInProcedure(Object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoginPage());
            NavigationPage.SetHasNavigationBar(this, false);
        }

    }
}
