﻿
using RelyFyCare.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;

namespace RelyFyCare.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {

        public static string strCaregiverName = "";
        public static string strClientId = "";
        public static string strEmployeeIdentifier = "";
        public static string strClientIdentifier = "";
        public static string strClientLocationId = "";
        public static string strEmployeeUUID = "";
        public static string strSessionId = "";




        public LoginPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        async Task SignInProcedureAsync(Object sender, EventArgs e)
        {

            User user = new User(Entry_Username.Text, Entry_Password.Text);

            if (user.CheckInformation())
            {

                //DisplayAlert("Login", "Login Success", "Okey!");

                await CallServiceAsync();

            }
            else
            {
                await DisplayAlert("Login", "Please Enter Username or Password", "Okey!");

            }
        }


        public async Task CallServiceAsync()
        {


            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
            new KeyValuePair<string,string>("emailId",Entry_Username.Text.ToString()),
            new KeyValuePair<string, string>("password",Entry_Password.Text.ToString()),
            new KeyValuePair<string, string>("deviceRegistrationId","0e9910392022")
        });


                var request = await cl.PostAsync(Constants.baseUrl + "/VeraPalRegistration/smartLogin?", formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponselogin res = JsonConvert.DeserializeObject<JsonResponselogin>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    // await DisplayAlert("Login", res.ResponseMessage, "Ok");

                    string sessionId = res.SessionId.ToString();
                    string employeeUniqueId = res.EmployeeUniqueId.ToString();
                   


                    foreach (var str in res.DataList)
                    {
                        strCaregiverName = str.name;
                         strClientId = str.clientId;
                         strClientLocationId = str.clientLocationId;
                        strEmployeeIdentifier = str.employeeIdentifier;
                         strClientIdentifier = str.clientIdentifier;
                        strEmployeeUUID = res.EmployeeUniqueId;
                        strSessionId = res.SessionId;

                        Application.Current.Properties["name"] = strCaregiverName;
                        Application.Current.Properties["clientId"] = strClientId;
                        Application.Current.Properties["clientLocationId"] = strClientLocationId;
                        Application.Current.Properties["employeeIdentifie"] = strEmployeeIdentifier;
                        Application.Current.Properties["clientIdentifier"] = strClientIdentifier;



                    }



                    Application.Current.Properties["sessionId"] = res.SessionId.ToString();
                    Application.Current.Properties["employeeUniqueId"] = res.EmployeeUniqueId.ToString();

                    await Navigation.PushModalAsync(new MainPage());
                    NavigationPage.SetHasNavigationBar(this, false);


                }
                else
                {
                    await DisplayAlert("Login", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }


        }

        private Task DisplayAlert(string v, string dataresponse)
        {
            throw new NotImplementedException();
        }


        private class JsonResponselogin
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }

            public string EmployeeUniqueId { get; set; }

            public string SessionId { get; set; }

            public LoginResponseModel[] DataList { get; set; }


        }
    }
}