﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RelyFyCare.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamForms.Controls;

namespace RelyFyCare.Views
{

    public partial class UnAvailable_Form : ContentPage
    {
        CalendarVM _vm;

        string strStartTimeDate = "";
        string strEntTimeDate = "";
        string strDate = "";
        string strEmployeeUUID = "";
        string strSessionId = "";
        string strEmployeeIdentifier = "";
        string strClientLocationId = "";
        string strClientUUID = "";
        string strClientIdentifier = "";
        string strName = "";
        string strStartTime = "";
        string strEndTime = "";
        string strStartDate = "";
        string strEndDate = "";

        public UnAvailable_Form()
        {
            InitializeComponent();

            //this.Title = "Unavailability";
             strEmployeeUUID = LoginPage.strEmployeeUUID;
             strSessionId = LoginPage.strSessionId;
             strEmployeeIdentifier = LoginPage.strEmployeeIdentifier;
             strClientLocationId = LoginPage.strClientLocationId;
             strClientUUID = LoginPage.strClientId;
             strClientIdentifier = LoginPage.strClientIdentifier;
             strName = LoginPage.strCaregiverName;

          

        }

        void SelectedDate(Object sender, EventArgs e){
            //Navigation.PushModalAsync(new UnAvailable_Status_Form());
            //NavigationPage.SetHasNavigationBar(this,true);
            string selectedDate = calendar.SelectedDate.Value.Date.ToString();

            this.calendar.MultiSelectDates = false;

            this.calendar.WeekdaysShow = true;
           

            DateTime dt=System.DateTime.Parse(selectedDate);
            string sr =  dt.ToString("MM-dd-yyyy");
            var me =   calendar.SelectedDate;
          
          
            strDate = sr.ToString();

          //  Application.Current.MainPage = new NavigationPage(new UnAvailable_Status_Form(sr));                
           // NavigationPage.SetHasNavigationBar(this, false);

        }


        void OnbtnSubmitAsync(object sender, EventArgs args)
        {

            //lbldisp.Text = string.Format("Thank you. Your appointment is set for {0}.", arrivalTimePicker.Time.ToString());

            strStartTimeDate = strDate + " " + arrivalTimePicker.Time.ToString();
            strEntTimeDate = strDate + " " + endTimePicker.Time.ToString();


            if (arrivalTimePicker.Time.ToString().Equals("12:00"))
            {
                DisplayAlert("Please select valid time", "Alert");
            }
            else if (endTimePicker.Time.Equals("12:00"))
            {

                DisplayAlert("Please select valid time", "Alert");
            }
            else
            {

                CallServiceAsync();

            }





        }




        private void DisplayAlert(string v1, string v2)
        {
            throw new NotImplementedException();
        }

        public async Task CallServiceAsync()
        {




            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("EmployeeUUID",strEmployeeUUID),
                    new KeyValuePair<string, string>("sessionId",strSessionId),
                    new KeyValuePair<string, string>("employeeIdentifier", strEmployeeIdentifier),
                    new KeyValuePair<string, string>("clientLocationId", strClientLocationId),
                    new KeyValuePair<string, string>("TimeZone",""),
                    new KeyValuePair<string, string>("AllDay",""),
                    new KeyValuePair<string, string>("StartDate",strStartTimeDate),
                    new KeyValuePair<string, string>("EndDate",strEntTimeDate),
                    new KeyValuePair<string, string>("Recurrence",""),
                    new KeyValuePair<string, string>("Note",""),
                    new KeyValuePair<string, string>("clientUUID", strClientUUID),
                    new KeyValuePair<string, string>("clientIdentifier", strClientIdentifier),
                    new KeyValuePair<string, string>("caregiverName", strName)



        });


                var request = await cl.PostAsync(Constants.baseUrl + Constants.sendUnAvailabilityData, formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseUnavailable res = JsonConvert.DeserializeObject<JsonResponseUnavailable>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    await DisplayAlert("UnAvailability", res.ResponseMessage, "Ok");
                   
                    CallServiceGetData();

                    //Application.Current.MainPage = new NavigationPage(new HomePage());
                    //NavigationPage.SetHasNavigationBar(this, false);


                }
                else
                {
                    await DisplayAlert("Open Shift", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }







        }


        public async Task CallServiceGetData()
        {

            using (var cl = new HttpClient())
            {
                var formcontent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("EmployeeUUID",strEmployeeUUID),
                    new KeyValuePair<string, string>("sessionId",strSessionId),
                    //new KeyValuePair<string, string>("employeeIdentifier", strEmployeeIdentifier),
                    new KeyValuePair<string, string>("clientLocationId", strClientLocationId),
                  



        });


                var request = await cl.PostAsync(Constants.baseUrl + Constants.getUnAvailabilityData, formcontent);

                request.EnsureSuccessStatusCode();

                var response = await request.Content.ReadAsStringAsync();
                // DisplayAlert("Response:- ",response);
                JsonResponseGetUnavailable res = JsonConvert.DeserializeObject<JsonResponseGetUnavailable>(response);
                //  await DisplayAlert("Login", "Okey!", res.responseMessage);
                string dataresponse = res.ResponseCode;

                if (dataresponse.Equals("102"))
                {

                    await DisplayAlert("UnAvailability", res.ResponseMessage, "Ok");


                    foreach (var str in res.dataList)
                    {
                        strStartTime = str.startTime;
                        strEndTime = str.endTime;
                        strStartDate = str.startDate;
                        strEndDate = str.endDate;
                       
                        //if (strStartDate.Equals()) { }

                    }

                    this.calendar.SelectedTextColor = Color.Green;
                  
                }




                    //Application.Current.MainPage = new NavigationPage(new HomePage());
                    //NavigationPage.SetHasNavigationBar(this, false);


                else
                {
                    await DisplayAlert("Unavailability", res.ResponseMessage, "Ok");
                }
                //  lbl1.Text = res.code + " " + res.status + " " + res.message;

            }
        }

        private class JsonResponseUnavailable
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }



            public LoginResponseModel[] list { get; set; }


        }


        private class JsonResponseGetUnavailable
        {

            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }



            public UnavailabilityResponseModel[] dataList { get; set; }


        }
    }
}

