﻿using RelyFyCare.MenuItem;
using RelyFyCare.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RelyFyCare
{
    public partial class MainPage : MasterDetailPage
    {

        public List<MasterPageItem> MenuList { get; set; }

        public MainPage()
        {
            InitializeComponent();

            MenuList = new List<MasterPageItem>();

            // Creating our pages for menu navigation
            // Here you can define title for item, 
            // icon on the left side, and page that you want to open after selection
            var page1 = new MasterPageItem() { Title = "My Calendar", Icon = "ic_home_2x.png", TargetType = typeof(HomePage) };
            var page2 = new MasterPageItem() { Title = "Open Shift", Icon = "ic_filter_tilt_shift_2x.png", TargetType = typeof(OpenShift_Form) };
            var page3 = new MasterPageItem() { Title = "Profile", Icon = "itemIcon2.png", TargetType = typeof(Profile_Form) };
            var page4 = new MasterPageItem() { Title = "Past Visit", Icon = "itemIcon6.png", TargetType = typeof(LastVisit_Form) };
            var page5 = new MasterPageItem() { Title = "Unavailability", Icon = "itemIcon5.png", TargetType = typeof(UnAvailable_Form) };
            var page6 = new MasterPageItem() { Title = "Change Password", Icon = "ic_lock_2x.png", TargetType = typeof(ChangePassword_Form) };
            var page7 = new MasterPageItem() { Title = "Share Relyfy", Icon = "ic_share_2x.png", TargetType = typeof(ShareRelyfy_Form) };
            var page8 = new MasterPageItem() { Title = "Show Notification", Icon = "ic_notifications_2x.png", TargetType = typeof(Notification_Form) };
            var page9 = new MasterPageItem() { Title = "Logout", Icon = "ic_exit_to_app_2x.png", TargetType = typeof(Logout_Form) };


            // Adding menu items to menuList
            MenuList.Add(page1);
            MenuList.Add(page2);
            MenuList.Add(page3);
            MenuList.Add(page4);
            MenuList.Add(page5);
            MenuList.Add(page6);
            MenuList.Add(page7);
            MenuList.Add(page8);
            MenuList.Add(page9);

            // Setting our list to be ItemSource for ListView in MainPage.xaml
            navigationDrawerList.ItemsSource = MenuList;

            // Initial navigation, this can be used for our home page
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(HomePage)));



        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;


        }
}
    }
